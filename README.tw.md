# KiCadPythonActionsPlugins

#### 介紹
 KiCad Python Actions Plugins
 KiCad Python 操作外掛

#### 軟體架構
軟體架構說明

本倉庫使用 `git submodule` 方式收集 KiCad 開源 Python 外掛合集

#### 安裝教程

1. 克隆本倉庫

```bash
git clone https://gitee.com/KiCAD-CN/KiCadPythonActionsPlugins.git
```

2. 使用子模組倉庫

```bash
// 初始化子模組倉庫
git submodule init 

// 更新子模組倉庫
git submodule update
```

或：

```bash
// 初始化和更新子模組倉庫組合命令
git submodule update --init --recursive
```

#### 使用說明

1. Windows 使用者安裝本倉庫 Python 外掛

- 需要安裝 [cmder](https://github.com/cmderdev/cmder) 終端環境
- 由於未知錯誤 cmder 官網無法訪問，請使用 [TUNA cmder 映象下載](https://mirrors.tuna.tsinghua.edu.cn/github-release/cmderdev/cmder/LatestRelease/cmder.7z) 
- 在 cmder 中執行自動安裝指令碼

```bash
./windowsinstall.bat
```

2. Linux 使用者安裝本倉庫 Python 外掛

- 在終端中執行自動安裝指令碼

```bash
./linuxinstall.sh
```

3. MacOS 使用者安裝本倉庫 Python 外掛

- 在終端中執行自動安裝指令碼

```bash
./macosinstall.sh
```

#### 自動安裝路徑說明

注：[plugins-name] 是外掛名稱，使用實際外掛名稱替代。

Windows 使用系統路徑安裝：

```bash
%appdata%/kicad/scripting/plugins/[plugins-name]
```

Linux 使用系統路徑安裝：

```bash
/usr/share/kicad/scripting/plugins/[plugins-name]
```

Mac OS (舊版) 使用系統路徑安裝：

```bash
~/Library/Application Support/kicad/scripting/plugins/[plugins-name]
```

Mac OS (新版)：使用系統路徑安裝：

```bash
~/Library/Preferences/kicad/scripting/plugins/[plugins-name]
```

#### 參與貢獻

1.  Fork 本倉庫
2.  新建 Feat_xxx 分支
3.  提交程式碼
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 來支援不同的語言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方部落格 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 這個地址來了解 Gitee 上的優秀開源專案
4.  [GVP](https://gitee.com/gvp) 全稱是 Gitee 最有價值開源專案，是綜合評定出的優秀開源專案
5.  Gitee 官方提供的使用手冊 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一檔用來展示 Gitee 會員風采的欄目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
