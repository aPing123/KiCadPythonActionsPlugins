# KiCadPythonActionsPlugins linuxinstall.sh
# coding UTF-8 Unix(LF)

# Shell 脚本在线检测和语法修复
# https://www.shellcheck.net/

# 1. 先确定当前所在绝对路径，再确定需要安装的路径。

# 2. 检测需要使用的命令行是否存在

# 3. 初始化并更新子模块仓库中的插件

# 4. 对需要安装依赖包子模块仓库安装相应的 Python 依赖包

# 5. 记录子模块的仓库的版本信息
#  使用 git describe --always | sed 's|-|.|g'
#  记录软件包版本信息并保存到当前路径下 pluginsinfo.txt

# 6. 自动安装到系统中 KiCad 的插件路径
#  稳定版安装路径：/usr/share/kicad/scripting/plugins/
#  测试版安装路径：/usr/share/kicad/5.99/scripting/plugins/

# 7. 安装插件软件包版本信息 pluginsinfo.txt 到相应的插件安装位置
#  稳定版：/usr/share/kicad/scripting/plugins/pluginsinfo.txt
#  测试版：/usr/share/kicad/5.99/scripting/plugins/pluginsinfo.txt

# 8. 自动更新子模块仓库并检测记录的版本信息是否一致
#  一致不更新
#  不一致更新

# 9. 卸载 KiCad Python 子模块插件
#  从插件信息里面读取安装的插件进行卸载或删除


# 获取当前目录决定路径
cur_dir=$(cd `dirname $0`; pwd)
# 稳定版安装路径
stable_dir='/usr/share/kicad/scripting/plugins'
# 测试版安装路径
test_dir='/usr/share/kicad/5.99/scripting/plugins'

local_pluginsinfo=${cur_dir}/pluginsinfo.txt
stable_pluginsinfo=${stable_dir}/pluginsinfo.txt
test_pluginsinfo=${test_dir}/pluginsinfo.txt

# 检测使用的命令行
check_command()
{
    local cmd = [ git pip pip3 kicad sed diff if pwd ls install ]
    for element in ${cmd[@]}
    # for element in ${cmd[*]}
    do
        if command -v $element > /dev/null 2>&1; then
        {
            type $element
        }
        else
        {
            echo "错误: $element 命令行没找到，请安装 $element"
            return 1
        }
        fi
    done
    return 0
}

# 更新子模块仓库
update_submodule()
{
    cd $cur_dir
    git submodule update --init --recursive
    return 0
}

#获取所有子模块仓库的文件夹名称
# display_submodule()
# {
    # local dir=`pwd`
    # local dirname_submodule=`ls $dir`
    # for element in ${dirname_submodule[@]}
    # do
    # echo $element 
    # done
# }

# 子模块仓库安装 Python 依赖包
dependency_submodule()
{
    pip3 install -r requirements.txt --user
    return 0
}

# 子模块仓库版本信息
describe_submodule()
{
    git describe --always
    return 0
}

# 生成 pluginsinfo.txt 
create_pluginsinfo()
{
#获取所有子模块仓库的文件夹名称
    local dir=`pwd`
    local dirname_submodule=`ls $dir`
    local describe
    local dirname_describe
    for element in ${dirname_submodule[@]}
    do
        cd $element 
        $describe=describe_submodule()
        $dirname_describe=${element}${describe}
        echo $dirname_describe >> $cur_dir/pluginsinfo.txt
    done
    return 0
}

# 读取 pluginsinfo.txt 信息
read_pluginsinfo()
{

}

# 比较 pluginsinfo.txt 信息
compare_pluginsinfo()
{
    diff ${local_pluginsinfo} ${stable_pluginsinfo} > /dev/null
    if [ $0 == 0 ]; then
        echo "Both file are same"
    else
        echo "Both file are different"
    fi
}

# 安装 pluginsinfo.txt 信息
install_pluginsinfo()
{
    if [ -d $stable_dir ]
    {
        install -Dm644 ${local_pluginsinfo} ${stable_pluginsinfo}
    }
    else
    {
        echo "没有找到 $stable_dir 目录"
    }
    fi
    if [ -d $test_dir ]
    {
        install -Dm644 ${local_pluginsinfo} ${test_pluginsinfo}
    }
    else
    {
        echo "没有找到 $test_dir 目录"
    }
    fi
}

# 稳定版或测试版安装 KiCad Python 操作插件
install_kicadplugin()
{

}

# 更新 KiCad Python 操作插件
update_kicadplugin()
{

}

# 删除 KiCad Python 操作插件
remove_kicadplugin()
{

}

help_kicadplugin(){
    echo "用法: linuxinstall.sh"
    echo "-i 安装 KiCad Python 操作插件到 KiCad 稳定版或测试版系统目录" 
    echo "-u 更新已安装的 KiCad Python 操作插件" 
    echo "-r 卸载已安装的 KiCad Python 操作插件" 
}

while getopts "h:itur" opt; do
    case $opt in
    i)
        install_kicadplugin
        ;;
    u)
        update_kicadplugin
        ;;
    r)
        remove_kicadplugin
        ;;
    h|?)
        help_kicadplugin
        ;;
    esac
done

# echo remaining parameters=[$@]
# echo \$0=[$0]
# echo \$1=[$1]
# echo \$2=[$2]
# echo \$3=[$3]
# echo \$4=[$4]


# ```bash
# %appdata%/kicad/scripting/plugins/[plugins-name]
# ```

# Linux 使用系统路径安装：

# ```bash
# /usr/share/kicad/scripting/plugins/[plugins-name]
# ```

# Mac OS (旧版) 使用系统路径安装：

# ```bash
# ~/Library/Application Support/kicad/scripting/plugins/[plugins-name]
# ```

# Mac OS (新版)：使用系统路径安装：

# ```bash
# ~/Library/Preferences/kicad/scripting/plugins/[plugins-name]
# ```
