# KiCadPythonActionsPlugins

#### Description
 KiCad Python Actions Plugins
 KiCad Python 操作插件

#### Software Architecture
Software architecture description

The repository uses `git submodule` to collect collections of KiCad open source Python plug-ins

#### Installation

1. Clone this repository. 

```bash
git clone https://gitee.com/KiCAD-CN/KiCadPythonActionsPlugins.git
```

2. Use the submodule repository. 

```bash. 
// initialize the submodule repository. 
git submodule init
// update sub-module repository. 
git submodule update
```

Or: 

```bash. 
// initialize and update sub-module repository combination command. 
git submodule update-- init-- recursive
```

#### Instructions
1. Windows users install the Python plug-in for this repository

- [cmder](https://github.com/cmderdev/cmder) terminal environment needs to be installed. 
- the official cmder website cannot be accessed due to an unknown error. Please use [TUNA cmder Image download](https://mirrors.tuna.tsinghua.edu.cn/github-release/cmderdev/cmder/LatestRelease/cmder.7z) 
- run the automatic installation script in cmder

```bash
./windowsinstall.bat
```

2. Linux users install the Python plug-in for this repository. 

- run the automatic installation script in the terminal

```bash
./linuxinstall.sh
```

3. MacOS users install the Python plug-in for this repository. 

-run the automatic installation script in the terminal

```bash
./macosinstall.sh
```

#### Automatic installation path description.
 
Note: [plugins-name] is the plug-in name, which is replaced by the actual plug-in name.

Windows is installed using the system path:

```bash
%appdata%/kicad/scripting/plugins/[plugins-name]
```

Linux is installed using the system path:

```bash
/usr/share/kicad/scripting/plugins/[plugins-name]
```

Mac OS (Legacy) install using system path:

```bash
~/Library/Application Support/kicad/scripting/plugins/[plugins-name]
```

Mac OS (New version) install using the system path:

```bash
~/Library/Preferences/kicad/scripting/plugins/[plugins-name]
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
