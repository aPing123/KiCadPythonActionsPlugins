# KiCadPythonActionsPlugins

#### 介绍
 KiCad Python Actions Plugins
 KiCad Python 操作插件

#### 软件架构
软件架构说明

本仓库使用 `git submodule` 方式收集 KiCad 开源 Python 插件合集

#### 安装教程

1. 克隆本仓库

```bash
git clone https://gitee.com/KiCAD-CN/KiCadPythonActionsPlugins.git
```

2. 使用子模块仓库

```bash
// 初始化子模块仓库
git submodule init 

// 更新子模块仓库
git submodule update
```

或：

```bash
// 初始化和更新子模块仓库组合命令
git submodule update --init --recursive
```

#### 使用说明

1. Windows 用户安装本仓库 Python 插件

- 需要安装 [cmder](https://github.com/cmderdev/cmder) 终端环境
- 由于未知错误 cmder 官网无法访问，请使用 [TUNA cmder 镜像下载](https://mirrors.tuna.tsinghua.edu.cn/github-release/cmderdev/cmder/LatestRelease/cmder.7z) 
- 在 cmder 中运行自动安装脚本

```bash
./windowsinstall.bat
```

2. Linux 用户安装本仓库 Python 插件

- 在终端中运行自动安装脚本

```bash
./linuxinstall.sh
```

3. MacOS 用户安装本仓库 Python 插件

- 在终端中运行自动安装脚本

```bash
./macosinstall.sh
```

#### 自动安装路径说明

注：[plugins-name] 是插件名称，使用实际插件名称替代。

Windows 使用系统路径安装：

```bash
%appdata%/kicad/scripting/plugins/[plugins-name]
```

Linux 使用系统路径安装：

```bash
/usr/share/kicad/scripting/plugins/[plugins-name]
```

Mac OS (旧版) 使用系统路径安装：

```bash
~/Library/Application Support/kicad/scripting/plugins/[plugins-name]
```

Mac OS (新版)：使用系统路径安装：

```bash
~/Library/Preferences/kicad/scripting/plugins/[plugins-name]
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
